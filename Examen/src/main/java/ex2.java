import javax.swing.*;
import java.io.File;

import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import java.io.*;


public class ex2 extends JFrame {

    JTextField textField1;
    JButton button1;


    ex2() {

        setTitle("Exercitiu 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;


        textField1 = new JTextField();
        textField1.setBounds(50, 50, width, height);

        button1 = new JButton("Citire");
        button1.setBounds(50, 100, width, height);


        button1.addActionListener(new Write());

        add(textField1);
        add(button1);
    }


    public static void main(String[] args) {
        new ex2();
    }

    class Write implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String filetext = ex2.this.textField1.getText();
            try {
                File f = new File("src/main/resources/" + filetext + ".txt");
                Scanner input = new Scanner(f);
                String result = "";
                while (input.hasNextLine()) {
                    String fjala = input.next();
                    for (int i = fjala.length() - 1; i >= 0; i--) {
                        result += fjala.charAt(i);
                    }
                    result += " ";
                }
                input.close();
                System.out.print(result + " ");
            } catch (FileNotFoundException ae) {
                System.out.println("An error occurred.");
                ae.printStackTrace();
            }

        }
    }

}
